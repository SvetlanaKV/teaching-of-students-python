import unittest
import lab4


class Testlab4(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.two = lab4.TwoVar(1, 2)

    def test_TwoVar_update(self):
        self.two.update(10, 20)
        self.assertEqual(self.two.var1, 10)
        self.assertEqual(self.two.var2, 20)

        self.two.update(22, 11)
        self.assertEqual(self.two.var1, 22)
        self.assertEqual(self.two.var2, 11)

    def test_TwoVar_sum(self):
        var1, var2 = 10, 20
        self.two.update(var1, var2)
        self.assertEqual(self.two.sum(), var1 + var2)

        var1, var2 = 155, -20
        self.two.update(var1, var2)
        self.assertEqual(self.two.sum(), var1 + var2)

    def test_TwoVar_max(self):
        var1, var2 = 10, 20
        self.two.update(var1, var2)
        self.assertEqual(self.two.max(), max(var1, var2))

        var1, var2 = 155, -20
        self.two.update(var1, var2)
        self.assertEqual(self.two.max(), max(var1, var2))


if __name__ == '__main__':
    unittest.main()
