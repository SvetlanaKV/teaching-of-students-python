import re


def file_in_out(infilename, outfilename='output_file.txt'):
    """
        В заданиях, оказывается, весь поток должен идти из файла в файл.
        Я это как-то проглядел и теперь пытаюсь выкрутиться,
        используя декорирущую функцию.
        Для этого же их придумали?
        Как следует не тестировал, но что есть читает. Как есть выводит.
    """
    def outer(func):
        with open(infilename) as f:
                text = f.read()

        def wrapper():
            result = func(text)

            with open(outfilename, 'w') as f:
                f.write(str(result))
        return wrapper
    return outer


# Дана строка.
# Вывести ее три раза через запятую и показать количество символов в ней
@file_in_out('io\\input.txt', 'io\\output.txt')
def three_str_and_len(s):
    """
        принимает строку, дублирует её три раза через запятую
        возвращает новую строку и её размер
    """
    s = ",".join([s]*3)
    return s, len(s)


# Дана строка. Вывести первый, последний и средний (если он есть)) символы.
def get_first_last_middle_symbol(s: str):
    """
        Возвращает первый, последний и средний(если он есть)
        символ переданной на вход строки
    """
    length = len(s)
    if length:
        return s[0], s[-1], s[length//2] if length % 2 != 0 else ''


# Дана строка. Если она начинается на 'abc', то заменить их на 'www',
# иначе добавить в конец строки 'zzz'.
def replace_abc(s: str) -> str:
    """
        Возвращает новую строку, в которой меняет abс на www.
        А если abc нет, то добавляет в конец строки zzz
    """
    if s.startswith("abc"):
        return s.replace("abc", 'www')
    return s + 'zzz'


# В данной строке найти количество цифр.
def len_digit(s: str) -> int:
    """Подсчитывает количество цифр в строке"""
    return len(re.findall('\\d', s))


# Найти самое длинное симметричное слово заданного предложения
def is_palindrome(s: str) -> bool:
    """Проверяет, является ли строка палиндромом"""
    s = s.lower()
    for i in range(len(s)):
        if s[i] != s[-(i+1)]:
            return False
    return True


def most_length_palindrome(text: str) -> str:
    """Находит в строке и возвращает самый длинный палиндром, если он есть"""
    words = re.findall('\\w+', text)
    max_i = -1
    max_len = 0

    for i, word in enumerate(words):
        print(is_palindrome(word))
        if is_palindrome(word):
            if len(word) > max_len:
                max_i = i
                max_len = len(words[max_i])

    if max_i > -1:
        return words[max_i]


rom_to_dec = {
    'I': 1,
    'V': 5,
    'X': 10,
    'L': 50,
    'C': 100,
    'D': 500,
    'M': 1000
}


def roman_number_to_decimal(rom_num: str) -> int:
    """Переводит римские цифры в арабские"""
    result, curr = 0, rom_to_dec['M']
    for ch in rom_num:
        prev = curr
        curr = rom_to_dec[ch]
        if curr > prev:
            result += curr - 2 * prev
        else:
            result += curr
    return result

dec_to_rom = {
    1000: 'M',
    900: 'CM',
    500: 'D',
    400: 'CD',
    100: 'C',
    90: 'XC',
    50: 'L',
    40: 'XL',
    10: 'X',
    9: 'IX',
    5: 'V',
    4: 'IV',
    1: 'I',
}


def decimal_to_roman_number(dec: int) -> str:
    """Переводит арабские цифры в римские"""
    dec = int(dec)
    result = ''
    for arabic, roman in dec_to_rom.items():
        result += dec // arabic * roman
        dec %= arabic
    return result
